package ru.t1.kruglikov.tm.service;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.hibernate.cfg.Environment;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.kruglikov.tm.api.service.IConnectionService;
import ru.t1.kruglikov.tm.api.service.IDatabaseProperty;
import ru.t1.kruglikov.tm.dto.model.ProjectDTO;
import ru.t1.kruglikov.tm.dto.model.SessionDTO;
import ru.t1.kruglikov.tm.dto.model.TaskDTO;
import ru.t1.kruglikov.tm.dto.model.UserDTO;
import ru.t1.kruglikov.tm.model.Project;
import ru.t1.kruglikov.tm.model.Session;
import ru.t1.kruglikov.tm.model.Task;
import ru.t1.kruglikov.tm.model.User;

import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityManager;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@Service
public final class ConnectionService implements IConnectionService {

    @NotNull
    @Autowired
    private final IDatabaseProperty databaseProperty;

    @Getter
    @NotNull
    @Autowired
    private final EntityManagerFactory entityManagerFactory;

    public ConnectionService(@NotNull final IDatabaseProperty databaseProperty) {
        this.databaseProperty = databaseProperty;
        entityManagerFactory = factory();
    }

    @NotNull
    private EntityManagerFactory factory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(org.hibernate.cfg.Environment.DRIVER, databaseProperty.getDatabaseDriver());
        settings.put(org.hibernate.cfg.Environment.URL, databaseProperty.getDatabaseUrl());
        settings.put(org.hibernate.cfg.Environment.USER, databaseProperty.getDatabaseLogin());
        settings.put(org.hibernate.cfg.Environment.PASS, databaseProperty.getDatabasePassword());
        settings.put(org.hibernate.cfg.Environment.DIALECT, databaseProperty.getDatabaseDialect());
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, databaseProperty.getDatabaseHBM2DDLAuto());
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, databaseProperty.getDatabaseShowSQL());
        settings.put(org.hibernate.cfg.Environment.FORMAT_SQL, databaseProperty.getDatabaseFormatSQL());
        settings.put(Environment.USE_SECOND_LEVEL_CACHE, databaseProperty.getDatabaseSecondLvlCash());
        settings.put(Environment.CACHE_REGION_FACTORY, databaseProperty.getDatabaseFactoryClass());
        settings.put(Environment.USE_QUERY_CACHE, databaseProperty.getDatabaseQueryCashKey());
        settings.put(Environment.USE_MINIMAL_PUTS, databaseProperty.getDatabaseUseMinPuts());
        settings.put(Environment.CACHE_REGION_PREFIX, databaseProperty.getDatabaseRegionPrefix());
        settings.put(Environment.CACHE_PROVIDER_CONFIG, databaseProperty.getDatabaseConfigFilePath());
       @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(UserDTO.class);
        sources.addAnnotatedClass(SessionDTO.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Session.class);
        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @NotNull
    @Override
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

}
