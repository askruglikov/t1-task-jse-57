package ru.t1.kruglikov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.kruglikov.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.kruglikov.tm.dto.model.UserDTO;
import ru.t1.kruglikov.tm.enumerated.UserSort;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public final class UserDtoRepository extends AbstractDtoRepository<UserDTO> implements IUserDtoRepository {

    @NotNull
    @Autowired
    public UserDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Nullable
    @Override
    public List<UserDTO> findAll() {
        @NotNull final String jpql = "SELECT m FROM UserDTO m ORDER by m.login";
        return entityManager.createQuery(jpql, UserDTO.class).getResultList();
    }

    @Nullable
    @Override
    public List<UserDTO> findAll(@Nullable UserSort sort) {
        @NotNull final String jpql = "SELECT m FROM UserDTO m ORDER BY " + sort.getColumnName();
        return entityManager.createQuery(jpql, UserDTO.class).getResultList();
    }

    @Nullable
    @Override
    public UserDTO findOneById(@NotNull final String id) {
        return entityManager.find(UserDTO.class, id);
    }

    @Nullable
    public UserDTO findByLogin(@NotNull final String login) {
        @NotNull final String jpql = "SELECT m FROM UserDTO m WHERE m.login = :login";
        return entityManager.createQuery(jpql, UserDTO.class)
                .setParameter("login", login)
                .setHint("org.hibernate.cacheable", true)
                .getSingleResult();
    }

    @Nullable
    public UserDTO findByEmail(@NotNull final String email) {
        @NotNull final String jpql = "SELECT m FROM UserDTO m WHERE m.email = :email";
        return entityManager.createQuery(jpql, UserDTO.class)
                .setParameter("email", email)
                .setHint("org.hibernate.cacheable", true)
                .getSingleResult();
    }

    @Override
    public void removeOneById(@NotNull final String id) {
        Optional<UserDTO> model = Optional.ofNullable(findOneById(id));
        model.ifPresent(this::remove);
    }

    @Override
    public void removeAll() {
        @NotNull final String jpql = "DELETE FROM UserDTO";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public long getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM UserDTO m";
        return entityManager.createQuery(jpql, Long.class).getSingleResult();
    }

}
